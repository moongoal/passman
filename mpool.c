/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "scramble.h"
#include "mpool.h"
#include "helper.h"

/* Write atomic value */
#define MPOOL_WRITE(pool, type, value) do { *((type *)((pool)->base + (pool)->size)) = (value); (pool)->size += sizeof(type); } while(0)

/* Write block of memory */
#define MPOOL_WRITE_BLOCK(pool, ptr, sz) do { memcpy((pool)->base + (pool)->size, ptr, sz); (pool)->size += sz; } while(0)

/* Initialize memory pool */
void mpool_init(MPOOL *pool) {
  pool->capacity = MPOOL_INIT_SIZE;
  pool->size = 0;
  pool->base = malloc(MPOOL_INIT_SIZE);
}

void mpool_free(MPOOL *pool) {
  sfree(pool->capacity, pool->base);
  memset(pool, 0, sizeof(MPOOL));
}

void mpool_ensure_size(MPOOL *pool, size_t sz) {
  if(pool->size + sz > pool->capacity) {
    pool->capacity += MPOOL_INCR_SIZE * ((size_t)ceil(sz / MPOOL_INCR_SIZE) + 1);
    pool->base = realloc(pool->base, pool->capacity);
  }
}

/* As is */
#define FUNC(type, name) void mpool_write_##name(MPOOL *pool, type value) {\
  mpool_ensure_size(pool, sizeof(type));\
  MPOOL_WRITE(pool, type, value);\
}

FUNC(uint8_t, u8);

#undef FUNC
/* Size-based unsigned integer with conversion to BE */
#define FUNC(sz) void mpool_write_u##sz(MPOOL *pool, uint##sz##_t value) {\
  mpool_ensure_size(pool, sizeof(uint##sz##_t));\
  MPOOL_WRITE(pool, uint##sz##_t, htobe##sz(value));\
}

FUNC(16);
FUNC(32);
FUNC(64);
#undef FUNC

void mpool_write_utf8(MPOOL *pool, const char *value) {
  size_t str_sz = strlen(value) * sizeof(char);

  mpool_write_u16(pool, (uint16_t)str_sz);
  mpool_write_block(pool, (uint8_t *)value, str_sz);
}

void mpool_write_block(MPOOL *pool, const uint8_t *block, size_t size) {
  mpool_ensure_size(pool, size);
  MPOOL_WRITE_BLOCK(pool, block, size);
}
