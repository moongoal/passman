/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <wchar.h>
#include <stdbool.h>
#include <string.h>
#include <termios.h>
#include "util.h"
#include "ansi.h"
#include "pdb.h"
#include "scramble.h"
#include "helper.h"
#include "debug.h"

#define SEARCH_TITLE 0x0001
#define SEARCH_USERNAME 0x0002
#define SEARCH_URL 0x0004
#define SEARCH_TAGS 0x0008
#define SEARCH_ALL (SEARCH_TITLE | SEARCH_USERNAME | SEARCH_URL | SEARCH_TAGS)

#define CMD_EXIT L"quit"
#define CMD_HELP L"help"
#define CMD_CREDENTIAL L"credential"
#define CMD_ADD_CREDENTIAL L"add-credential"
#define CMD_NEW L"new"
#define CMD_SET_USERNAME L"set-username"
#define CMD_SET_PASSWORD L"set-password"
#define CMD_SET_URL L"set-url"
#define CMD_SET_TAGS L"set-tags"
#define CMD_SHOW L"show"
#define CMD_SHOW_ALL L"show-all"
#define CMD_ADD_TAG L"add-tag"
#define CMD_REMOVE_TAG L"remove-tag"
#define CMD_SET_TITLE L"set-title"
#define CMD_ADD_DATA L"add-data"
#define CMD_LIST_DATA L"list-data"
#define CMD_REMOVE_DATA L"remove-data"
#define CMD_SHOW_DATA L"show-data"
#define CMD_SAVE_DATA L"save-data"
#define CMD_LIST L"list"
#define CMD_SEARCH L"search"
#define CMD_SETSEARCH L"set-search"
#define CMD_SAVE L"save"
#define CMD_LOAD L"open"
#define CMD_CLOSE L"close"
#define CMD_REMOVE L"delete"
#define CMD_SET_DBPASS L"set-db-password"
#define CMD_CLEAR L"clear"

static uint16_t sflags = SEARCH_ALL; /* Search flags */
static bool modified = false; /* Has the opened database been modified? */
static PDB_DATABASE *db = NULL; /* Open DB */
static PDB_CREDENTIAL *cred = NULL; /* Selected credential */

static wchar_t *readlinew(const wchar_t *prompt) {
  #define READLINE_MAXBUFF 2048
  wchar_t *buff = malloc(sizeof(wchar_t) * READLINE_MAXBUFF);

  clearerr(stdin);
  fputws(prompt, stdout);
  fgetws(buff, READLINE_MAXBUFF, stdin);

  if(!feof(stdin))
    buff[wcslen(buff) - 1] = L'\0'; /* Remove newline */
  else {
    sfree(sizeof(wchar_t) * READLINE_MAXBUFF, buff);
    buff = NULL;
    clearerr(stdin);
  }

  return buff;
  #undef READLINE_MAXBUFF
}

static void repl_help(const wchar_t *cmd) {
  static wchar_t *help[] = {
    CMD_EXIT, L"Exit the program", NULL,
    CMD_HELP, L"Show this message", NULL,
    CMD_NEW, L"Create a new database", L"DATABASE_PATH",
    CMD_CREDENTIAL, L"Get or set the current credential", L"[CREDENTIAL_TITLE]",
    CMD_ADD_CREDENTIAL, L"Create and select a new credential", L"CREDENTIAL_TITLE",
    CMD_SET_USERNAME, L"Set username", L"NEW_USERNAME",
    CMD_SET_PASSWORD, L"Set password", NULL,
    CMD_SET_URL, L"Set URL", L"NEW_URL",
    CMD_SET_TAGS, L"Set tags", L"NEW_TAGS",
    CMD_SHOW, L"Show credential (hidden password)", NULL,
    CMD_SHOW_ALL, L"Show credential (visible password)", NULL,
    CMD_ADD_TAG, L"Append a tag to a credential", L"TAG",
    CMD_REMOVE_TAG, L"Remove a tag from a credential", L"TAG",
    CMD_SET_TITLE, L"Set credential title", L"TITLE",
    CMD_ADD_DATA, L"Add custom data to credential", L"{string|file} NAME STRING_CONTENT_OR_FILENAME",
    CMD_LIST_DATA, L"List custom data records", NULL,
    CMD_REMOVE_DATA, L"Remove a data record from credential", L"NAME",
    CMD_SHOW_DATA, L"Show a string data record", L"NAME",
    CMD_SAVE_DATA, L"Save a file data record", L"NAME PATH",
    CMD_LIST, L"List credentials (with pager, if -p is supplied)", L"[-p]",
    CMD_SEARCH, L"Search credentials by title, username tags and URL", L"TEXT_TO_LOOK_FOR",
    CMD_SETSEARCH, L"Set search fields", L"[all || ([title|username|url|tags],...)]",
    CMD_SAVE, L"Save the current database to file", L"[PATH]",
    CMD_LOAD, L"Load a database from file", L"PATH",
    CMD_CLOSE, L"Save and close the current database (-n to not save)", L"[-n]",
    CMD_REMOVE, L"Remove the current credential from the database", L"[NAME]",
    CMD_SET_DBPASS, L"Change database password", NULL,
    CMD_CLEAR, L"Clear screen", NULL,
    NULL
  };

  if(cmd == NULL) { /* General help */
    wchar_t **h = help;
    wchar_t *hname = *h, *hdesc = *(h + 1);

    while(hname != NULL) {
      wprintf(ANSI_CUR_SAVE L"%ls" ANSI_CUR_RESTORE ANSI_CUR_FW(32) L"%ls\n", hname, hdesc);

      h += 3;
      hname = *h;
      hdesc = *(h + 1);

    }
  } else { /* Single command help */
    /* Find command */
    wchar_t **c = help;

    while(*c != NULL) {
      if(wcscmp(*c, cmd) == 0) { /* Found command */
        wchar_t *desc = *(c + 1);
        wchar_t *args= *(c + 2);

        wprintf(L"Command: %ls\n\t%ls\n", cmd, desc);

        if(args)
          wprintf(L"Usage:\n\t%ls %ls\n", cmd, args);

        break;
      }
      c += 3;
    }

    if(*c == NULL) { /* Not found */
      wprintf(L"No help for command %ls\n", cmd);
    }
  }
}

/*
  Reads and returns password from stdin.

  Returned value must be scramble-freed by application.
*/
static wchar_t *repl_readpass(const wchar_t *prompt) {
  int fdin = fileno(stdin);
  struct termios tin, tin_orig;
  wchar_t *out;

  tcgetattr(fdin, &tin_orig);
  memcpy(&tin, &tin_orig, sizeof(struct termios));

  tin.c_lflag ^= ECHO;

  tcsetattr(fdin, TCSANOW, &tin);
  out = readlinew(prompt);
  tcsetattr(fdin, TCSANOW, &tin_orig);

  fputws(L"\n", stdout);
  return out;
}

/*
  Reads (twice) and returns password from stdin if valid, otherwise returns NULL.

  Returned value must be scramble-freed by application.
*/
static wchar_t *repl_readpass2(const wchar_t *prompt1, const wchar_t *prompt2) {
  while(true) {
    wchar_t *pw1 = repl_readpass(prompt1? prompt1: L"Password: ");
    wchar_t *pw2;
    
    if(!pw1) {
      return NULL;
    } else {
      pw2 = repl_readpass(prompt2? prompt2: L"Repeat password: ");

      if(!pw2) {
        return NULL;
      } else {
        if(wcscmp(pw1, pw2) != 0) {
          sfree(sizeof(wchar_t) * (1 + wcslen(pw1)), pw1);
          pw1 = NULL;
          fputws(L"Passwords don't match\n", stdout);
        }

        sfree(sizeof(wchar_t) * (1 + wcslen(pw2)), pw2);

        if(pw1)
          return pw1;
        else
          continue;
      }
    }
  }
}

/* Asks the user a yes/no question and returns the result */
UNUSED static bool yesno(const wchar_t *prompt) {
  wchar_t *choice;
  wchar_t c;
  
  do {
    choice = readlinew(prompt);
  } while(wcscmp(choice, L"y") != 0 && wcscmp(choice, L"n") != 0);

  c = choice[0];
  free(choice);

  return c == L'y';
}

/*
  True if the file does not exist or exists and the user tells to override,
  false if the file is a directory or the user tells not to override.
*/
static bool file_allowed(const wchar_t *path) {
  struct stat s;
  char *spath = to_multibyte(path);
  int sout;
  bool result = false;

  errno = 0;
  sout = stat(spath, &s);

  if(sout == 0 || errno == ENOENT) {
    if(errno == ENOENT)
      result = true;
    else {
      if(!S_ISDIR(s.st_mode)) {
        if(yesno(L"The destination file already exists. Overwrite? (y/n) "))
          result = true;
      } else
        error(L"Path is a directory");
    }
  } else
    error_errno();

  free(spath);
  return result;
}

/*
  Checks and returns whether a DB is currently open.
*/
static bool repl_check_db() {
  if (db == NULL) {
    error(L"No database opened");
    return false;
  } else return true;
}

/*
  Checks and returns whether a credential is currently selected.
*/
static bool repl_check_cred() {
  if (cred == NULL) {
    error(L"No credential selected");
    return false;
  } else return true;
}

static void repl_remove_credential(wchar_t *name) {
  if(repl_check_db()) {
    PDB_CREDENTIAL *c = name? pdb_get_credential(db, name): cred;

    if(c) {
      pdb_remove_credential(db, c);

      if(c == cred)
        cred = NULL;

      modified = true;
    } else {
      if(!name)
        repl_help(CMD_REMOVE);
      else
        error(L"No credential found with the given name");
    }
  }
}

static void repl_new_db(const wchar_t *path) {
  if(path == NULL) {
    repl_help(CMD_NEW);
    return;
  }

  if(!db) {
    wchar_t *pass;

    if(!file_allowed(path))
      return;

    pass = repl_readpass2(L"New database password: ", L"Repeat database password: ");

    if(!pass) {
      fputws(L"Abort\n", stdout);
    } else {
      if(wcscmp(pass, L"") != 0) {
        wchar_t *pth = expand_tilde(path);

        if(!(db = pdb_new(pth, pass)))
          error(L"Unable to create new database");
        else
          modified = true;

        free(pth);
      } else {
        error(L"Empty passwords not allowed");
      }
      sfree(wcslen(pass) * sizeof(wchar_t), pass);
    }
  } else
    error(L"A database is already open");
}

/*
  Cleans allocated data.
*/
static void repl_clean() {
  if(db)
    pdb_free(db);

  db = NULL;
  cred = NULL;
  modified = false;
}

/*
  Returns false if the REPL interface must terminate and cleans data.

  Returns true and doesn't clean anything if the REPL interface must not
  terminate.
*/
static bool repl_exit() {
  #ifndef DEBUG
  const wchar_t *qprompt = modified? L"Unsaved changes will be lost. Exit passman? (y/n) ": L"Exit passman? (y/n) ";

  if(feof(stdin))
    fputws(L"\n", stdout);

  if(yesno(qprompt)) {
    repl_clean();
    return false;
  } else {
    return true;
  }
  #else /* DEBUG */
    repl_clean();
    return false;
  #endif /* DEBUG */
}

static void repl_add_credential(const wchar_t *title) {
  if(repl_check_db()) {
    if(!title)
      repl_help(CMD_ADD_CREDENTIAL);
    else {
      PDB_CREDENTIAL *c = pdb_new_credential(db, title);

      if(!c)
        error(L"A credential with the same name already exists");
      else {
        cred = c;
        modified = true;
      }
    }
  }
}

static void repl_credential(const wchar_t *title) {
  if(repl_check_db()) {
    if(!title) {
      if(!cred)
        fputws(L"No credential selected\n", stdout);
      else {
        wchar_t *title = pdb_get_credential_title(db, cred);
        wprintf(L"Selected credential: %ls\n", title);
        free(title);
      }
    } else {
      PDB_CREDENTIAL *c = pdb_get_credential(db, title);

      if(!c)
        wprintf(L"No credential named \"%ls\"", title);
      else
        cred = c;
    }
  }
}

static void repl_set_username(const wchar_t *value) {
  if(repl_check_db()) {
    if(!value)
      value = L"";

    if(repl_check_cred()) {
      wchar_t *old = pdb_get_credential_username(db, cred);

      if(wcscmp(old, value) != 0) {
        pdb_set_credential_username(db, cred, value);
        wprintf(L"%ls => %ls\n", wcscmp(old, L"") == 0? L"(None)": old, wcscmp(value, L"") == 0? L"(None)": value);
        modified = true;
      }

      sfree(sizeof(wchar_t) * (1 + wcslen(old)), old);
    }
  }
}

static void repl_set_db_password() {
  if(repl_check_db()) {
    wchar_t *pass = repl_readpass2(L"New database password: ", L"Repeat database password: ");

    if(!pass) {
      puts("Abort");
    } else {
      if(wcscmp(pass, L"") != 0) {
        pdb_set_key(db, pass);

        fputws(L"New database password set\n", stdout);
        modified = true;
      } else {
        error(L"Empty passwords not allowed");
      }
      sfree(wcslen(pass) * sizeof(wchar_t), pass);
    }
  }
}

static void repl_set_password() {
  if(repl_check_db()) {
    if(repl_check_cred()) {
      wchar_t *pw = repl_readpass2(NULL, NULL);

      if(pw) {
        wchar_t *oldpw = pdb_get_credential_password(db, cred);

        if(wcscmp(pw, oldpw) != 0) {
          pdb_set_credential_password(db, cred, pw);
          modified = true;
        }

        sfree(sizeof(wchar_t) * (1 + wcslen(pw)), pw);
        sfree(sizeof(wchar_t) * (1 + wcslen(oldpw)), oldpw);
      } else
        fputws(L"Abort\n", stdout);
    }
  }
}

static void repl_set_title(const wchar_t *value) {
  if(value) {
    if(repl_check_db())
      if(repl_check_cred()) {
        if(!pdb_get_credential(db, value)) { /* Not existing, we can rename it */
          pdb_set_credential_title(db, cred, value);
          modified = true;
        } else { /* Found another cred with the same name. Oops! */
          error(L"Another credential with the same name exists.");
        }
      }
  } else
    repl_help(CMD_SET_TITLE);
}

static void repl_set_url(const wchar_t *value) {
  if(repl_check_db()) {
    if(!value)
      value = L"";

    if(repl_check_cred()) {
      wchar_t *old = pdb_get_credential_url(db, cred);

      if(wcscmp(old, value) != 0) {
        pdb_set_credential_url(db, cred, value);
        wprintf(L"%ls => %ls\n", wcscmp(old, L"") == 0? L"(None)": old, wcscmp(value, L"") == 0? L"(None)": value);
        modified = true;
      }

      sfree(sizeof(wchar_t) * (1 + wcslen(old)), old);
    }
  }
}

static void repl_set_tags(const wchar_t *value) {
  if(repl_check_db()) {
    if(!value)
      value = L"";

    if(repl_check_cred()) {
      wchar_t *old = pdb_get_credential_tags(db, cred);

      if(wcscmp(old, value) != 0) {
        pdb_set_credential_tags(db, cred, value);
        wprintf(L"%ls => %ls\n", wcscmp(old, L"") == 0? L"(None)": old, wcscmp(value, L"") == 0? L"(None)": value);
        modified = true;
      }

      sfree(sizeof(wchar_t) * (1 + wcslen(old)), old);
    }
  }
}

static void repl_add_tag(const wchar_t *tagname) {
  if(tagname == NULL) {
    repl_help(CMD_ADD_TAG);
    return;
  }

  if(repl_check_db())
    if(repl_check_cred()) {
      wchar_t *tags, *tags2, *newtags, *state, *ptr;
      size_t tags_sz, newtags_len;

      /* Check no commas in tagname */
      if(wcschr(tagname, L',') != NULL) { /* Found comma */
        error(L"Commas not allowed in tags");
        return;
      }

      tags = pdb_get_credential_tags(db, cred);

      /* Check if new tag is already present */
      tags_sz = sizeof(wchar_t) * (wcslen(tags) + 1);
      tags2 = malloc(tags_sz);
      wcscpy(tags2, tags);

      for(ptr = wcstok(tags2, L",", &state); ptr != NULL; ptr = wcstok(NULL, L",", &state)) {
        if(wcscmp(tagname, ptr) == 0) { /* Tag already present */
          fputws(L"Tag already present\n", stdout);
          free(tags2);
          free(tags);
          return;
        }
      }
      free(tags2);

      /* Add tag */
      if(wcscmp(tags, L"") != 0) {
        newtags_len = wcslen(tags) + 1 + wcslen(tagname);
        newtags = malloc(sizeof(wchar_t) * (1 + newtags_len));
        swprintf(newtags, newtags_len + 1, L"%ls,%ls", tags, tagname);
        pdb_set_credential_tags(db, cred, newtags);
        wprintf(L"%ls => %ls\n", tags, newtags);
        free(newtags);
        modified = true;
      } else { /* Empty tag set */
        repl_set_tags(tagname);
      }

      free(tags);
    }
}

static void repl_remove_tag(const wchar_t *tagname) {
  if(tagname) {
    if(repl_check_db())
      if(repl_check_cred()) {
        wchar_t *tags, *newtags, *ptr, *state;
        bool changed = false;

        tags = pdb_get_credential_tags(db, cred);
        newtags = calloc(sizeof(wchar_t), 2 /* Take into account trailing '\0' and comma */ + wcslen(tags));
        
        for(ptr = wcstok(tags, L",", &state); ptr != NULL; ptr = wcstok(NULL, L",", &state))
          if(wcscmp(ptr, tagname) != 0) { /* Not the tag to remove */
            wcscat(newtags, ptr);
            wcscat(newtags, L",");
          } else
            changed = true;

        if(changed) {
          newtags[wcslen(newtags)-1] = L'\0';
          repl_set_tags(newtags);
        } else
          fputws(L"Tag not found\n", stdout);

        free(newtags);
        free(tags);
      }
  } else
    repl_help(CMD_REMOVE_TAG);
}

static void repl_remove_data(const wchar_t *name) {
  if(name) {
    if(repl_check_db())
      if(repl_check_cred()) {
        PDB_DATA *d = pdb_get_data(cred, name);

        if(d) {
          size_t i;

          for(i = 0; i < cred->data->length; i++)
            if(cred->data->arr[i]->val.ptr == d) {
              qc_al_remove_index(cred->data, i);

              pdb_data_clear_content(d);
              free(d);

              modified = true;
              break;
            }
        } else
          fputws(L"No data record with that name\n", stdout);
      }
  } else
    repl_help(CMD_REMOVE_DATA);
}

static void add_string_data(PDB_DATA *data, const wchar_t *value) {
  pdb_set_data_text(db, data, value);

  modified = true;
}

static void add_file_data(PDB_DATA *data, const wchar_t *name, const wchar_t *filename) {
  FILE *f;
  char *mb_filename = to_multibyte(filename);
  struct stat s;

  if(stat(mb_filename, &s) == 0) {
    if(S_ISDIR(s.st_mode)) {
      error(L"Can't add directories");
      repl_remove_data(name);
      free(mb_filename);
      return;
    }
  }

  if((f = fopen(mb_filename, "rb"))) {
    fclose(f);
    pdb_set_data_file(db, data, filename);

    modified = true;
  } else {
    error_errno();
    repl_remove_data(name);
  }

  free(mb_filename);
}

static void add_data_error() {
  error(L"A data record with the same name already exists");
}

static void repl_add_data(const wchar_t *dtype, const wchar_t *dname, const wchar_t *ddata) {
  PDB_DATA *data;

  if(repl_check_db())
    if(repl_check_cred()) {
      if(!dtype || !dname || !ddata)
        repl_help(CMD_ADD_DATA);
      else if(wcscmp(dtype, L"string") == 0) {
        data = pdb_new_data(db, cred, dname, PDB_DATA_TYPE_TEXT);

        if(data)
          add_string_data(data, ddata);
        else
          add_data_error();
      } else if(wcscmp(dtype, L"file") == 0) {
        data = pdb_new_data(db, cred, dname, PDB_DATA_TYPE_FILE);

        if(data)
          add_file_data(data, dname, ddata);
        else
          add_data_error();
      }
      else
        repl_help(CMD_ADD_DATA);
    }
}

static void show_credential(bool show_password) {
  if(repl_check_db())
    if(repl_check_cred()) {
      wchar_t *title, *user, *pass, *url, *tags;

      title = pdb_get_credential_title(db, cred);
      user = pdb_get_credential_username(db, cred);
      pass = pdb_get_credential_password(db, cred);
      url = pdb_get_credential_url(db, cred);
      tags = pdb_get_credential_tags(db, cred);

      wprintf(L"CREDENTIAL %ls\n", title);
      wprintf(
        ANSI_CUR_SAVE L"Username:" ANSI_CUR_RESTORE ANSI_CUR_FW(24) L"%ls\n"
        ANSI_CUR_SAVE L"Password:" ANSI_CUR_RESTORE ANSI_CUR_FW(24) L"%ls\n"
        ANSI_CUR_SAVE L"URL:" ANSI_CUR_RESTORE ANSI_CUR_FW(24) L"%ls\n"
        ANSI_CUR_SAVE L"Tags:" ANSI_CUR_RESTORE ANSI_CUR_FW(24) L"%ls\n",
        user, show_password? pass: L"**********", url, tags);

      sfree(sizeof(wchar_t) * (1 + wcslen(title)), title);
      sfree(sizeof(wchar_t) * (1 + wcslen(user)), user);
      sfree(sizeof(wchar_t) * (1 + wcslen(pass)), pass);
      sfree(sizeof(wchar_t) * (1 + wcslen(url)), url);
      sfree(sizeof(wchar_t) * (1 + wcslen(tags)), tags);
    }
}

static void repl_show() {
  show_credential(false);
}

static void repl_show_all() {
  show_credential(true);
}

static void repl_list_data() {
  if(repl_check_db())
    if(repl_check_cred()) {
      QC_AL *al = cred->data;

      if(al->length == 0)
        fputws(L"No data records\n", stdout);
      else
        for(size_t i = 0; i < al->length; i++) {
          PDB_DATA *data = al->arr[i]->val.ptr;
          wchar_t *name = pdb_get_textentry_value(db, &data->name, false);

          wprintf(L"- [%lc] %ls\n",
            data->type == PDB_DATA_TYPE_TEXT? L'T': L'F', /* text or file */
            name);
          free(name);
        }
    }
}

static void repl_show_data(const wchar_t *name) {
  if(name) {
    if(repl_check_db())
      if(repl_check_cred()) {
        PDB_DATA *d = pdb_get_data(cred, name);

        if(d) {
          if(d->type == PDB_DATA_TYPE_TEXT) {
            wchar_t *txt = pdb_get_data_text(db, d);
            
            wprintf(L"%ls\n", txt);
            sfree((wcslen(txt) + 1) * sizeof(wchar_t), txt);
          } else
            error(L"This record is not a text record");
        } else
          error(L"No data record with that name");
      }
  } else
    repl_help(CMD_SHOW_DATA);
}

static void repl_save_data(const wchar_t *name, const wchar_t *path) {
  if(name && path) {
    if(repl_check_db())
      if(repl_check_cred()) {
        PDB_DATA *d = pdb_get_data(cred, name);

        if(d) {
          if(d->type == PDB_DATA_TYPE_FILE) {
            if(file_allowed(path)) {
              errno = 0;
              pdb_get_data_file(db, d, path);

              if(errno != 0)
                error_errno();
            }
          } else
            error(L"This record is not a file record");
        } else
          error(L"No data record with that name");
      }
  } else
    repl_help(CMD_SAVE_DATA);
}

static void repl_list(const wchar_t *mod) {
  if(repl_check_db()) {
    PDB_CREDENTIAL *c;
    FILE *sout = stdout; /* output stream */

    if(mod)
      if(wcscmp(mod, L"-p") == 0) { /* use pager */
        char *pager = getenv("PAGER");

        if(!pager)
          pager = "less";

        errno = 0;
        sout = popen(pager, "w");

        if(!sout) {
          wprintf(L"WARNING: %s\n", strerror(errno));
          sout = stdout;
        }
      }

    for(size_t i = 0; i < db->ecreds->length; i++) {
      wchar_t *t;

      c = db->ecreds->arr[i]->val.ptr;
      t = pdb_get_credential_title(db, c);

      if(sout != stdout)
        fprintf(sout, "- %ls\n", t);
      else
        fwprintf(sout, L"- %ls\n", t);
      free(t);
    }

    if(sout != stdout)
      pclose(sout);
  }
}

static void repl_search(const wchar_t *what) {
  if(what) {
    if(repl_check_db()) {
      PDB_CREDENTIAL *c;

      for(size_t i = 0; i < db->ecreds->length; i++) {
        c = db->ecreds->arr[i]->val.ptr;
        wchar_t *title = pdb_get_credential_title(db, c);

        if(sflags & SEARCH_TITLE)
          if(ws_isearch(title, what)) {
            wprintf(L"- %ls\n", title);
            continue;
          }

        #define SRC_CRED(flag, target) if(sflags & flag) {\
          wchar_t *s = pdb_get_credential_##target(db, c);\
          \
          if(ws_isearch(s, what)) { wprintf(L"- %ls\n", title); sfree((1 + wcslen(s)) * sizeof(wchar_t), s); continue; }\
          sfree((1 + wcslen(s)) * sizeof(wchar_t), s); }

        SRC_CRED(SEARCH_USERNAME, username)
        SRC_CRED(SEARCH_URL, url)
        SRC_CRED(SEARCH_TAGS, tags)

        #undef SRC_CRED
      }
    }
  } else
    repl_help(CMD_SEARCH);
}

static void repl_search_opts(wchar_t *opts) {
  if(opts) { /* Set */
    wchar_t *s;
    wchar_t *state;
    wchar_t *sep = L",";
    uint16_t oldflags = sflags;

    sflags = 0;

    for(s = wcstok(opts, sep, &state); s != NULL; s = wcstok(NULL, sep, &state)) {
      if(wcscmp(s, L"username") == 0)
        sflags |= SEARCH_USERNAME;
      else if(wcscmp(s, L"title") == 0)
        sflags |= SEARCH_TITLE;
      else if(wcscmp(s, L"url") == 0)
        sflags |= SEARCH_URL;
      else if(wcscmp(s, L"tags") == 0)
        sflags |= SEARCH_TAGS;
      else if(wcscmp(s, L"all") == 0)
        sflags = SEARCH_ALL;
      else {
        fwprintf(stdout, L"ERROR: Unknown search flag \"%ls\"\n", s);
        sflags = oldflags;
        break;
      }
    }
  } else { /* Print */
    wprintf(L"The `" CMD_SEARCH L"` command will look through the following fields:\n");

    #define PRINTOPT(flag, field) if(sflags & flag) { wprintf(L"- %s\n", field); }
    PRINTOPT(SEARCH_TITLE, "title")
    PRINTOPT(SEARCH_USERNAME, "username")
    PRINTOPT(SEARCH_URL, "url")
    PRINTOPT(SEARCH_TAGS, "tags")
    #undef PRINTOPT
  }

}

static void repl_save(const wchar_t *path) {
  if(repl_check_db()) {
    if(path) { /* Set new path */
      wchar_t *pth = expand_tilde(path);

      if(!file_allowed(pth))
        return;

      free(db->path);
      db->path = pth;
    } else if(!modified) {
      fputws(L"Not modified\n", stdout);
      return;
    }

    if(pdb_save(db)) {
      wprintf(L"Saved to %ls\n", db->path);
      modified = false;
    } else
      error(L"Error while saving database; stored data might be corrupt.\n");
  }
}

static void repl_load(const wchar_t *path) {
  if(path) {
    wchar_t *pth = expand_tilde(path);

    if(db == NULL) {
      char *spath = to_multibyte(pth);
      struct stat s;

      if(stat(spath, &s) == 0) {
        if(!S_ISDIR(s.st_mode)) {
          wchar_t *pass = repl_readpass(L"Password: ");

          if(pass) {
            errno = 0;
            db = pdb_load(pth, pass);

            /* Yeah, these two are redundant but fuck life, I'm paranoid */
            modified = false;
            cred = NULL;

            free(pass);

            if(!db) {
              if(errno != 0)
                error_errno();
              else
                error(L"Unable to load database");
            }
          }
        }
      } else
        error_errno();

      free(spath);
    } else
      error(L"Another database is open");

    free(pth);
  } else
    repl_help(CMD_LOAD);
}

static void repl_clear() {
  fputws(ANSI_ERASE_DISPLAY ANSI_CUR_POS(0, 0), stdout);
}

static void repl_close(const wchar_t *arg) {
  if(repl_check_db()) {
    if(arg)
      if(wcscmp(arg, L"-n") == 0)
        modified = false;

    if(modified)
      repl_save(NULL);

    pdb_free(db);
    cred = NULL;
    db = NULL;
    modified = false;
  }
}

/*
  Interprets a single REPL line.

  Returns true if the REPL interpreter should continue or false if the
  interface must exit.
*/
bool repl_interpret(wchar_t *cmdline) {
  wchar_t *state, *cmd = wcstok(cmdline, L" ", &state);

  #define NEXTARG wcstok(NULL, L" ", &state)
  #define REMAINING state
  #define _if_cmd(c) 
  #define if_cmd(c) if(wcscmp(cmd, c) == 0)

  if(cmd == NULL)
    return true;

  if(
    feof(stdin)
    || wcscmp(cmd, CMD_EXIT) == 0
  )
    return repl_exit();

  else if_cmd(CMD_HELP)
    repl_help(NEXTARG);

  else if_cmd(CMD_CREDENTIAL)
    repl_credential(REMAINING);

  else if_cmd(CMD_ADD_CREDENTIAL)
    repl_add_credential(REMAINING);

  else if_cmd(CMD_NEW)
    repl_new_db(REMAINING);

  else if_cmd(CMD_SET_USERNAME)
    repl_set_username(REMAINING);

  else if_cmd(CMD_SET_PASSWORD)
    repl_set_password();

  else if_cmd(CMD_SET_URL)
    repl_set_url(REMAINING);

  else if_cmd(CMD_SET_TAGS)
    repl_set_tags(NEXTARG);

  else if_cmd(CMD_SHOW)
    repl_show();

  else if_cmd(CMD_SHOW_ALL)
    repl_show_all();

  else if_cmd(CMD_ADD_TAG)
    repl_add_tag(NEXTARG);

  else if_cmd(CMD_REMOVE_TAG)
    repl_remove_tag(NEXTARG);

  else if_cmd(CMD_SET_TITLE)
    repl_set_title(REMAINING);

  else if_cmd(CMD_ADD_DATA) {
    wchar_t *dtype = NEXTARG;
    wchar_t *dname = NEXTARG;
    wchar_t *dcontent = REMAINING;

    repl_add_data(dtype, dname, dcontent);
  }
  
  else if_cmd(CMD_LIST_DATA)
    repl_list_data();

  else if_cmd(CMD_REMOVE_DATA)
    repl_remove_data(NEXTARG);

  else if_cmd(CMD_SHOW_DATA)
    repl_show_data(NEXTARG);

  else if_cmd(CMD_SAVE_DATA) {
    wchar_t *name = NEXTARG;
    wchar_t *path = REMAINING;

    repl_save_data(name, path);
  }

  else if_cmd(CMD_LIST)
    repl_list(NEXTARG);

  else if_cmd(CMD_SEARCH)
    repl_search(REMAINING);

  else if_cmd(CMD_SETSEARCH)
    repl_search_opts(REMAINING);

  else if_cmd(CMD_SAVE)
    repl_save(REMAINING);

  else if_cmd(CMD_LOAD)
    repl_load(REMAINING);

  else if_cmd(CMD_CLOSE)
    repl_close(NEXTARG);

  else if_cmd(CMD_REMOVE)
    repl_remove_credential(REMAINING);

  else if_cmd(CMD_SET_DBPASS)
    repl_set_db_password();
  else if_cmd(CMD_CLEAR)
    repl_clear();

  else { /* Unknown command */
    error(L"Unknown command");
  }

  #undef if_cmd
  #undef NEXTARG

  return true;
}

void repl_run(wchar_t *dbfile) {
  bool proceed = true;

  if(dbfile)
    repl_load(dbfile);

  while(proceed) {
    wchar_t *cmd = readlinew(L"passman> ");

    if(cmd != NULL) {
      proceed = repl_interpret(cmd);
      free(cmd);
    } else proceed = false;

    fputws(L"\n", stdout);
  }
}
