/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifndef _PDB_H
#define _PDB_H

#include <stdint.h>
#include <stdbool.h>
#include <qcollect/alist.h>
#include "encrypt.h"

/*
  Size in bytes of in-memory temporary encryption key.
*/
#define PDB_MEMKEY_SIZE 64

/*
  These macros expand to pdb_set_textentry_value() and are used to correctly
  set the corresponding value.

  ARGUMENTS
    db: The DB instance
    c: The credential instance
    value: The value to be set

  RETURN VALUE
    Same as pdb_set_textentry_value() for the corresponding entry
*/
#define pdb_set_credential_url(db, c, value) pdb_set_textentry_value((db), &(c)->url, (value), false)
#define pdb_set_credential_tags(db, c, value) pdb_set_textentry_value((db), &(c)->tags, (value), false)
#define pdb_set_credential_username(db, c, value) pdb_set_textentry_value((db), &(c)->username, (value), true)
#define pdb_set_credential_password(db, c, value) pdb_set_textentry_value((db), &(c)->password, (value), true)
#define pdb_set_credential_title(db, c, value) pdb_set_textentry_value((db), &(c)->title, (value), false)

/*
  These macros expand to pdb_get_textentry_value() and are used to correctly
  get the corresponding value.

  ARGUMENTS
    db: The DB instance
    c: The credential instance

  RETURN VALUE
    Same as pdb_get_textentry_value() for the corresponding entry
*/
#define pdb_get_credential_url(db, c) pdb_get_textentry_value((db), &(c)->url, false)
#define pdb_get_credential_tags(db, c) pdb_get_textentry_value((db), &(c)->tags, false)
#define pdb_get_credential_username(db, c) pdb_get_textentry_value((db), &(c)->username, true)
#define pdb_get_credential_password(db, c) pdb_get_textentry_value((db), &(c)->password, true)
#define pdb_get_credential_title(db, c) pdb_get_textentry_value((db), &(c)->title, false)

/* Credential data type */
typedef enum {
  PDB_DATA_TYPE_TEXT, /* Text data */
  PDB_DATA_TYPE_FILE /* File data */
} PDB_DATA_TYPE;

/*
  Database text entry. This is useful because some strings are encrypted, thus
  you can't rely on line terminators to determine their length.

  Saved on file as:
    - size (as uint16_t)
    - *ptr (encoded as UTF-8)
*/
typedef struct {
  size_t size; /* String size in bytes (including the line terminator) */
  wchar_t *ptr;
} PDB_TEXTENTRY;

/*
  Password DataBase in-memory representation.

  Saved on file as:
    - ctr
    - salt
    - iterations
    - ecreds (encrypted)
*/
typedef struct {
  wchar_t *path; /* DB path */
  uint8_t ctr[ENCRYPT_BLOCKSIZE]; /* CTR */
  uint8_t salt[ENCRYPT_SALTSIZE]; /* Salt */
  uint8_t *key; /* Encrypted user-supplied database encryption key */
  size_t keysz; /* `key` size in bytes */
  uint8_t memkey[PDB_MEMKEY_SIZE]; /* Random temporary key used for in-memory encryption */
  uint32_t iterations; /* Key hashing iterations */
  QC_AL *ecreds; /* Array list of pointers to encrypted credential entries (PDB_CREDENTIAL) */
} PDB_DATABASE;

/*
  Saved on file as:
    - title
    - username
    - password
    - url
    - tags
    - data (# of entries as uint16_t and then actual data)
*/
typedef struct {
  PDB_TEXTENTRY title, username /* encrypted */, password /* encrypted */, url, tags;
  QC_AL *data;
} PDB_CREDENTIAL;

/*
  Saved on file as:
    - name
    - type (as uint16_t)
    - size
    - value
*/
typedef struct {
  PDB_TEXTENTRY name;
  PDB_DATA_TYPE type;
  uint64_t size;
  void *value; /* encrypted */
} PDB_DATA;

/*
  Gets a PDB_TEXTENTRY's value.

  ARGUMENTS
    db: The DB instance
    t: The PDB_TEXTENTRY instance
    decrypt: True if the value has to be decrypted

  RETURN VALUE
    A string containing the text entry's value. The application is responsible
    scramble-freeing the string after usage
*/
wchar_t *pdb_get_textentry_value(const PDB_DATABASE *db, const PDB_TEXTENTRY *t, bool decrypt);

/*
  Sets a PDB_TEXTENTRY's value.

  ARGUMENTS
    db: The DB instance
    t: The PDB_TEXTENTRY instance
    value: The value to assign to the text entry
    encrypt: True if the value has to be encrypted

  RETURN VALUE
    The value `t` is returned
*/
PDB_TEXTENTRY *pdb_set_textentry_value(const PDB_DATABASE *db, PDB_TEXTENTRY *t, const wchar_t *value, bool encrypt);

/*
  Gets a PDB_DATA instance pointer based on exact data name match.

  ARGUMENTS
    c: Pointer to the credential instance
    name: Name to look for

  RETURN VALUE
    A pointer to the PDB_DATA instance or NULL if the given title is not found
*/
PDB_DATA* pdb_get_data(const PDB_CREDENTIAL *c, const wchar_t *name);

/*
  Creates and returns a new associated credential data, appended to the
  database.

  ARGUMENTS
    db: Pointer to the DB instance
    c: Pointer to the credential instance
    name : Name of new data instance

  RETURN VALUE
    A new PDB_DATA instance pointer or NULL if a data record with the same name
    already exists for the same credential
*/
PDB_DATA* pdb_new_data(const PDB_DATABASE *db, const PDB_CREDENTIAL *c, const wchar_t *name, PDB_DATA_TYPE type);

/*
  Gets a PDB_CREDENTIAL instance pointer based on exact credential title match.

  ARGUMENTS
    db: Pointer to the DB instance
    title: Title to look for

  RETURN VALUE
    A pointer to the PDB_CREDENTIAL instance or NULL if the given title is not
    found
*/
PDB_CREDENTIAL* pdb_get_credential(const PDB_DATABASE *db, const wchar_t *title);

/*
  Creates and returns a new credential, appended to the database.

  ARGUMENTS
    db: Pointer to the DB instance
    title: Title of new credential instance

  RETURN VALUE
    A new PDB_CREDENTIAL instance pointer or NULL if a credential with the
    same name already exists
*/
PDB_CREDENTIAL* pdb_new_credential(const PDB_DATABASE *db, const wchar_t *title);

/*
  Removes a credential from the database.

  ARGUMENTS
    db: Pointer to the DB instance
    c: Pointer to the credential instance to remove
*/
void pdb_remove_credential(PDB_DATABASE const *db, PDB_CREDENTIAL *c);

/*
  Allocates and initializes a new empty database.

  ARGUMENTS
    path: The new DB path
    key: The plaintext key
  
  RETURN VALUE
    The new database instance. No file is created as this point. The returned
    structure must be freed by calling pdb_free()
*/
PDB_DATABASE* pdb_new(const wchar_t *path, const wchar_t *key);

/*
  Loads a database from file.

  ARGUMENTS
    path: The database path
    key: The plaintext database decryption key

  RETURN VALUE
    A new database instance filled with the data gathered from the file at the
    given path. The returned structure must be freed by calling pdb_free().
    On error, NULL is returned
*/
PDB_DATABASE *pdb_load(const wchar_t *path, const wchar_t *key);

/*
  Saves a database to file.

  ARGUMENTS
    db: Pointer to the database instance

  RETURN VALUE
    True if the file has successfully been saved, false if not
*/
bool pdb_save(PDB_DATABASE const *db);

/*
  Cleans any data associated with the database instance and releases any memory
  associated with it. The `db` pointer is invalidated after call to this
  function.

  ARGUMENTS
    db: Pointer to the database instance to free
*/
void pdb_free(PDB_DATABASE *db);

/*
  Encrypts data by using the encrypt module. Use only for in-memory encrypted
  storage.

  ARGUMENTS
    db: The PDB_DATABASE instance to use
    sz: The size of the input and output data buffers
    data: The data to encrypt
    out: The ouput buffer that will receive the encrypted data
*/
void pdb_crypt(PDB_DATABASE const *db, size_t sz, uint8_t const *data, uint8_t *out);

/*
  Returns the decrypted key as a null-terminated string.

  ARGUMENTS
    db: The database instance to retrieve the key from

  RETURN VALUE
    A pointer to a null-terminated string representing the key. The application
    is responsible for scramble-freeing the pointer when done with it.
*/
wchar_t *pdb_getkey(PDB_DATABASE const *db);

/*
  Sets a PDB text data value.

  ARGUMENTS
    db: The PDB_DATABASE instance
    d: The PDB_DATA instance
    value: The text data to set
*/
void pdb_set_data_text(PDB_DATABASE const *db, PDB_DATA *d, const wchar_t *value);

/*
  Returns the text data associated with a PDB credential data value.

  ARGUMENTS
    db: The PDB_DATABASE instance
    d: The PDB_DATA instance

  RETURN VALUE
    A null-terminated string representing the given data text. The application
    is responsible for scramble-freeing the pointer when done with it.
*/
wchar_t *pdb_get_data_text(PDB_DATABASE const *db, PDB_DATA *d);

/*
  Sets a PDB file data value.

  ARGUMENTS
    db: The PDB_DATABASE instance
    d: The PDB_DATA instance
    path: Path of the file to store
*/
void pdb_set_data_file(PDB_DATABASE const *db, PDB_DATA *d, const wchar_t *path);

/*
  Saves the PDB_DATA file data to a file on the filesystem.

  ARGUMENTS
    db: The PDB_DATABASE instance
    d: The PDB_DATA instance
    path: Path of the file to store
*/
void pdb_get_data_file(PDB_DATABASE const *db, PDB_DATA *d, const wchar_t *path);

/*
  Clears the content of a PDB data entry.

  ARGUMENTS
    d: A pointer to a PDB_DATA instance

  NOTES
    The pointer is not freed.
*/
void pdb_data_clear_content(PDB_DATA *d);

/*
  Sets the user-supplied database encryption key.

  ARGUMENTS
    db: Pointer to the PDB_DATABASE instance
    key: Pointer to the plain user-supplied encryption key
*/
void pdb_set_key(PDB_DATABASE *db, const wchar_t *key);

#endif /* _PDB_H */
