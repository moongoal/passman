/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <wchar.h>
#include <wctype.h>
#include <stdlib.h>
#include <iconv.h>
#include <limits.h>
#include <sys/types.h>
#include <pwd.h>
#include <unistd.h>
#include "util.h"
#include "debug.h"
#include "scramble.h"

bool fexists(const wchar_t *path) {
  FILE *f;
  char *spath = to_multibyte(path);

  errno = 0;

  if((f = fopen(spath, "r")))
    fclose(f);

  free(spath);

  return f != NULL;
}

char *to_multibyte(const wchar_t *ws) {
  size_t sz = wcstombs(NULL, ws, 0) + 1;

  if(sz > 0) { /* Correct size returned */
    char *s = malloc(sz * sizeof(char));
    wcstombs(s, ws, sz);

    return s;
  } else /* Charset incompatibility, abort */
    fatal(L"Locale character set does not support character; switching to Unicode is advised");
}

wchar_t *from_multibyte(const char *s) {
  size_t sz = mbstowcs(NULL, s, 0) + 1;

  if(sz > 0) { /* Correct size returned */
    wchar_t *ws = malloc(sz * sizeof(wchar_t));
    mbstowcs(ws, s, sz);

    return ws;
  } else /* Invalid multibyte sequence, abort */
    fatal(L"Invalid multibyte sequence");
}

char *to_utf8(const wchar_t *ws) {
  iconv_t ctx = iconv_open("UTF8", "WCHAR_T");

  if(ctx != (iconv_t)-1) {
    size_t wlen = wcslen(ws);
    size_t wsz = wlen * sizeof(wchar_t);
    size_t sz = (wlen + 1) * sizeof(char) * 6 /* max UTF-8 code point size is 6 chars, let's assume worst case */; /* Size in bytes of output buffer */
    char *s = calloc(1, sz);
    wchar_t *win = malloc(wsz + sizeof(wchar_t));
    wchar_t *win_orig = win;
    char *s_orig = s;

    wcscpy(win, ws);
    iconv(ctx, (char **)&win, &wsz, &s, &sz);

    iconv_close(ctx);
    sfree((wlen + 1) * sizeof(wchar_t), win_orig);

    return s_orig;
  } else
    fatal(L"Unable to open iconv context for conversion to UTF-8");
}

wchar_t *from_utf8(const char *s) {
  iconv_t ctx = iconv_open("WCHAR_T", "UTF8");

  if(ctx != (iconv_t)-1) {
    size_t len = strlen(s);
    size_t sz = len * sizeof(char);
    size_t wsz = (len + 1) * sizeof(wchar_t); /* Size in bytes of output buffer */
    wchar_t *ws = calloc(1, wsz);
    char *in = malloc(sz + sizeof(char));
    char *in_orig = in;
    wchar_t *ws_orig = ws;

    strcpy(in, s);
    iconv(ctx, &in, &sz, (char **)&ws, &wsz);

    iconv_close(ctx);
    sfree((len + 1 ) * sizeof(char), in_orig);

    return ws_orig;
  } else
    fatal(L"Unable to open iconv context for conversion from UTF-8");
}

wchar_t *ws_tolower(const wchar_t *in) {
  size_t len = wcslen(in);
  wchar_t *out = malloc((len + 1) * sizeof(wchar_t));

  for(size_t i = 0; i <= len; i++)
    out[i] = (wchar_t)towlower(in[i]);

  return out;
}

bool ws_isearch(const wchar_t *in, const wchar_t* what) {
  wchar_t *iin = ws_tolower(in), *iwhat = ws_tolower(what);
  bool result = wcsstr(in, what) != NULL;

  free(iin);
  free(iwhat);

  return result;
}

wchar_t *expand_tilde(const wchar_t *path) {
  if(path[0] == L'~') { /* Perform tilde expansion */
    struct passwd pwd, *result = NULL;
    #define BUFF_LEN (LOGIN_NAME_MAX + 2*PATH_MAX + 1024)
    char *buff = malloc(BUFF_LEN * sizeof(char));
    wchar_t *retval = NULL;

    if(path[1] != L'/') { /* User name specified */
      wchar_t path2[PATH_MAX + 1], *user_w_status, *user_w;
      char *user = NULL;

      wcscpy(path2, path);

      if((user_w = wcstok(&path2[1], L"/", &user_w_status)) != NULL) {
        user = to_utf8(user_w);
      
        getpwnam_r(user, &pwd, buff, BUFF_LEN, &result);
        free(user);
      }
    } else /* Default to current user name */
      getpwuid_r(getuid(), &pwd, buff, BUFF_LEN, &result);

    if(result == &pwd) { /* Got a result */
      wchar_t *homedir = from_multibyte(pwd.pw_dir);
      wchar_t *relpath; /* Path relative to home directory */

      if((relpath = wcschr(path, L'/')) != NULL) {
        size_t retval_len = (1 + wcslen(relpath) + wcslen(homedir));
        retval = malloc(retval_len * sizeof(wchar_t));

        swprintf(retval, retval_len, L"%ls%ls", homedir, relpath);
      }
      
      free(homedir);
    }

    free(buff);

    return retval;
  } else
    return wcsdup(path);

  #undef BUFF_LEN
}
