/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifndef _DEBUG_H
#define _DEBUG_H

#include <string.h>
#include <wchar.h>
#include <errno.h>

/* Fail compilation if static condition is not met */
#define _STATIC_ASSERT(cond, file, line) typedef char _assert_##file##_##line[(cond)? 1: -1];
#define STATIC_ASSERT(cond) _STATIC_ASSERT((cond), __FILE__, __LINE__)

/*
  Print an error displaying a string message relative to current errno value.
*/
void error_errno();

/*
  Print an error message to STDERR and exit with error.
*/
void fatal(const wchar_t *msg) __attribute__((noreturn));
void mbfatal(const char *msg) __attribute__((noreturn));

/*
  Print an error message to STDERR.
*/
void error(const wchar_t *msg);

/*
  Print a warning message to STDERR.
*/
void warning(const wchar_t *msg);

#endif /* _DEBUG_H */
