/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifndef _SCRAMBLE_H
#define _SCRAMBLE_H

#include <stddef.h>

#define SCRAMBLE_DEFAULT_ITERATIONS 1

/*
  Scrambles the memory and frees the pointer (single iteration).

  ARGUMENTS
    sz: Size of memory location pointed by `ptr`
    ptr: Pointer to memory location to be scrambled and freed

  NOTES
    Needs the random module to be initialized before usage
*/
#define sfree(sz, ptr) sfree2(sz, ptr, SCRAMBLE_DEFAULT_ITERATIONS)

/*
  Scrambles the memory and frees the pointer (multiple iterations).

  ARGUMENTS
    sz: Size of memory location pointed by `ptr`
    ptr: Pointer to memory location to be scrambled and freed
    n: Number of scramble iterations before freeing

  NOTES
    Needs the random module to be initialized before usage
*/
void sfree2(size_t sz, void *ptr, size_t n);

/*
  Scrambles the memory but does not free the pointer.

  ARGUMENTS
    sz: Size of memory location pointed by `ptr`
    ptr: Pointer to memory location to be scrambled
    n: Number of scramble iterations

  RETURN VALUE
    The same value passed as `ptr`.

  NOTES
    Needs the random module to be initialized before usage
*/
void *scramble(size_t sz, void *ptr, size_t n);

#endif /* _SCRAMBLE_H */
