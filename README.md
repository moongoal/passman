PASSMAN: Password Manager
=====
Passman is a password manager featuring both file and in-memory encryption.
In-memory encryption refers to the practice of encrypting data temporarily
stored in primary memory, while file encryption refers to the practice of
encrypting data before writing it to file, protecting it with a passphrase to
avoid unauthorized access.

## Encryption
Passman encrypts its sensitive data with the Twofish algorithm in GCM mode. The
given passphrase is used to generate a cryptographically secure encryption key
with PBKDF2-Whirlpool. The cryptographic routines are supplied by the gcrypt
library.

### In-Memory Encryption
In-memory encryption is used to make unauthorized data access harder while the
application is running. Everytime a database is loaded, a new randomly
generated key is used to encrypt sensitive information. When that information is
used, it must be decrypted; passman scrambles any allocated memory that might
contain sensitive clear-text information before releasing it by overriding its
contents with random data.

## Interface
Passman has a REPL interface. It reads commands from the standard input and
prints their results on the standard output. It's been designed to work on
headless environments like servers so at present it lacks a GUI.

## Help System
The program contains an integrated help system (the _help_ command) which
contains help information about every command and a list of available commands.

## Operating Systems
At present the only supported OS family is the GNU/Linux one. Support for
OpenBSD might come soon.

## Project status
The project is currently alpha software.

### Capabilities
The passman binary performs memory lock (by calling _mlockall_) for security
purposes. Be sure it has the *IPC_LOCK* capability or it might crash on startup.
