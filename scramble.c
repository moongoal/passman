/*
 *    Passman: Password Manager
 *    Copyright (C) 2016  Alfredo Mungo <alfredo.mungo@openmailbox.org>
 *
 *    This program is free software; you shall redistribute it and/or modify
 *    it under the terms of the GNU General Public License version 2 as
 *    published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#include <stdlib.h>
#include <gcrypt.h>
#include "scramble.h"

void sfree2(size_t sz, void *ptr, size_t n) {
  free(scramble(sz, ptr, n));
}

void *scramble(size_t sz, void *ptr, size_t n) {
  for(register size_t i = 0; i < n; i++)
    gcry_randomize(ptr, sz, GCRY_WEAK_RANDOM);

  return ptr;
}
